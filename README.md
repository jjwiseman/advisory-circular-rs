<a href="https://gitlab.com/jjwiseman/advisory-circular-rust/-/pipelines">
<img alt="pipeline status"
     src="https://gitlab.com/jjwiseman/advisory-circular-rust/badges/master/pipeline.svg">
</a>

# Advisory Circular

This is Rust code for a twitter bot that monitors all aircraft in a specified
geographic area and tweets when it detects an aircraft circling.

There are about 24 instances of this bot that are active; most of them are run
by me but a few are run by other people. They cover Los Angeles, the San
Francisco Bay Area, New York City, Ottawa, London, Chicago and many other areas.
You can find most of them in the [Advisory Circular bots twitter
list](https://twitter.com/i/lists/1263724487435890688).

![Screenshot of Advisory Circular bot tweet](adv-cir-screenshot.jpg)

The bot uses the following sources of data:

* The [ADS-B Exchange](https://www.adsbexchange.com/) API provides real-time
  aircraft data like position, speed, altitude, and squawk code. ADS-B Exchange
  is primarily a network of amateurs using SDRs to receive and decode aircraft
  transponder signals.
* Screenshots of the [ADS-B Exchange map](https://globe.adsbexchange.com/) are
  taken to show a map of the aircraft and its track.
* [Airport-Data.com](https://www.airport-data.com/) provides photos of aircraft.
* [OpenStreetMap](https://www.openstreetmap.org/) (via a local
  [Pelias](https://pelias.io/) instance) has geographic data like
  county/city/neighborhoods boundaries and names, airport locations, and
  landmarks.
* A copy of the [mictronics.de aircraft
  database](https://www.mictronics.de/aircraft-database/) provides aircraft type
  and registration information.

## Tweet criteria

The bot doesn't track any specific aircraft, or type of aircraft. It only cares
about an aircraft's behavior: Is it circling?

"Circling" is, however, used in a rather loose sense. The bot just looks for
1440 (4 * 360) degrees of heading change over the past 25 minutes. It actually
needs to be a _net_ heading change of 1440 degrees; left turns cancel out right
turns and vice-versa. This means that the bots catch a lot of other interesting
behavior that isn't strictly circling.

Once an aircraft has been identified as "circling", there are a couple other
conditions that need to be met before it will be tweeted:

1. The centroid of the last 3 minutes of reported positions must be more than
   2.5 km away from all known airports. This is an attempt to filter out planes
   doing pattern practice.

2. Once an aircraft has been tweeted, it must stop circling for at least 30
   minutes before it's eligible to be tweeted again.

## Development history

The [first version of this
bot]((https://gitlab.com/jjwiseman/advisory-circular)) was written in
Clojurescript. I rewrote it in Rust for performance reasons. Load on the machine
that runs the bots went from about 5 for the Clojurescript version to
approximately 0 for the Rust version. You can read more details about the
rewrite in [this twitter
thread](https://twitter.com/lemonodor/status/1455906997727993858).

## How to run a copy of this bot for yourself

It's all easy except for pelias, the geo database. That part is hard unless you
happen to be a pelias expert, or there's a [pre-built pelias docker
image](https://github.com/pelias/docker) for the particular geographic area you
want to run a bot in. In any case, I can try to help you; just ask me on
twitter, where I'm [@lemonodor](https://twitter.com/lemonodor).

### Build the software

To create the `advisorycircular` executable:
```
cargo build --release
```

### Create configuration files

Here's the config file I use for the Los Angeles area. I keep it in
`/etc/advisorycircular/la-config.yml`

```yaml
adsbx:
  # url: https://adsbexchange.com/api/aircraft/v2/
  # My local ADSBX API proxy.
  url: http://localhost:8000/api/aircraft/v2/
pelias:
  # My local pelias instance.
  url: http://lockheed.local:4000/v1
lat: 34.1335
lon: -118.1924
radius-km: 50
max-history-age-ms: 3600000
landmarks:
  blocklist:
    # Don't mention Johnny Depp's house (see https://twitter.com/lemonodor/status/1440430260856578055)
    - Johnny.Depp
icao-blocklist:
  # Some sort of beacon, see e.g. https://twitter.com/SkyCirclesLA/status/1287057451309654017
  - ADF7F9
twitter:
  enabled?: true
airport:
  blocklist:
    # These are either not really airports or are closed.
    - Puente Sky Ranch
    - Radio Control
    - Shepherd Field
```

Secrets are kept in a separate file, `/etc/advisorycircular/la-secrets.yml`:

```yaml
adsbx:
  api-key: <your api key>
twitter:
    consumer-key: <your consumer key>
    consumer-secret: <your consumer secret>
    access-token: <your access token>
    access-token-secret: <your access token secret>
```

Note that this does not support the new, paid ADS-B Exchange API. You can google
how to create a twitter app and get its credentials.

### Create `systemd` file

I find it most convenient to run the bot using systemd unit files. Because I run
dozens of bots I create one template file that can be used for all the bots. The
file is located at `/lib/systemd/system/advisorycircular@.service`.

In the unit file, `%i` is a variable that gets replaced with the ID for each
bot, which is just a short string like `la`, `nyc`, etc.

```ini
[Unit]
Description=Advisory Circular twitter bot for %i
Documentation=https://gitlab.com/jjwiseman/advisory-circular/
Wants=network.target
After=network.target

[Service]
Type=simple
User=advisorycircular
Restart=always
RestartSec=30s
ExecStart=/usr/local/share/advisorycircular/advisorycircular %i

[Install]
WantedBy=multi-user.target
```

`/usr/local/share/advisorycircular/advisorycircular` is a script I use that runs
the `advisorycircular` binary with the appropriate arguments for the specific
bot given the bot ID:

```sh
#!/bin/bash
set -e

dir=$(dirname "$0")
instance="$1"
config_dir=/etc/advisorycircular
run_dir=/var/lib/advisorycircular
ac_db_path=/usr/local/share/advisorycircular/aircraft-info/aircraft-info.sqb
interval=15

config_path="$config_dir"/"$instance"-config.yaml
secrets_path="$config_dir"/"$instance"-secrets.yaml
history_path="$run_dir"/"$instance"-history.json
(cd "$run_dir" &&
 RUST_BACKTRACE=1 "$dir"/intervalexec $interval $dir/rs/advisorycircular --config "$config_path" \\
                   --secrets "$secrets_path" --aircraft-info-db "$ac_db_path" --history "$history_path" \\
                   --log-prefix "$instance")
```
