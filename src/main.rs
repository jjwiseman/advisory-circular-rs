use advisorycircular::{
    adsbx::{screenshot, AdsbxApi},
    aircraft::AircraftState,
    aircraft_db,
    airport_data::get_photo,
    app_state::AppState,
    base_filename,
    config::{AppConfig, Secrets},
    errors::Error,
    fires, pelias,
    pelias::{Boundary, NearbyOptions, PeliasApi},
    track_to_geojson,
    twitter::{interactively_get_oauth_credentials, tweet},
};
use anyhow::Context;
use chrono::{DateTime, Duration, Utc};
use fern::colors::{Color, ColoredLevelConfig};
use geo::Point;
use itertools::Itertools;
use log::{debug, error, info, warn};
use std::{collections::HashMap, fs::File, io::Write, str::FromStr, time::Instant};
use structopt::StructOpt;

const MIN_CURVINESS: f64 = 1440.0;

#[derive(StructOpt, Debug)]
struct CliArgs {
    #[structopt(
        long,
        default_value = "advisorycircular.json",
        value_name = "file",
        help = "History file"
    )]
    history: String,
    #[structopt(
        long,
        default_value = "config.yaml",
        value_name = "file",
        help = "Configuration file"
    )]
    config: String,
    #[structopt(
        long,
        default_value = "secrets.yaml",
        value_name = "file",
        help = "Secrets file"
    )]
    secrets: String,
    #[structopt(long, value_name = "prefix", help = "Log prefix to use")]
    log_prefix: Option<String>,
    #[structopt(
        long,
        default_value = "aircraft-info.sqb",
        value_name = "file",
        help = "Aircraft database file"
    )]
    aircraft_info_db: String,
    #[structopt(
        long,
        takes_value = false,
        help = "Create a new aircraft database by importing Mictronics.de JSON and then exit"
    )]
    create_aircraft_info_db_from_json: Option<String>,
    #[structopt(
        long,
        help = "Look up an ICAO in the aircraft database and exit.",
        value_name = "icao"
    )]
    lookup_aircraft: Option<String>,
    #[structopt(
        long,
        takes_value = false,
        help = "Get a twitter authentication token and exit."
    )]
    get_twitter_auth_token: bool,
    #[structopt(
        long,
        help = "Simulate a tweet for the specified aircraft and exit.",
        value_name = "hex"
    )]
    simulate: Option<String>,
}

fn setup_logger(prefix: &Option<String>) -> Result<(), fern::InitError> {
    let colors = ColoredLevelConfig::new().info(Color::Green);
    let prefix = prefix
        .as_ref()
        .map(|p| format!(" {:5} ", p))
        .unwrap_or_default();
    fern::Dispatch::new()
        .format(move |out, message, record| {
            // out.finish(format_args!(
            //     "{}{}[{}] {}",
            //     chrono::Local::now().format("[%Y-%m-%d][%H:%M:%S]"),
            //     prefix,
            //     colors.color(record.level()),
            //     message
            // ))
            out.finish(format_args!(
                "{}[{}] {}",
                prefix,
                colors.color(record.level()),
                message
            ))
        })
        .level(log::LevelFilter::Info)
        .chain(std::io::stdout())
        .apply()?;
    Ok(())
}

fn read_config(path: &str) -> anyhow::Result<AppConfig> {
    debug!("Loading config from {}", path);
    Ok(AppConfig::from_file(path)?)
}

fn read_secrets(path: &str) -> anyhow::Result<Secrets> {
    debug!("Loading secrets from {}", path);
    Ok(Secrets::from_file(path)?)
}

// Prunes the list of aircraft to keep only ones we've seen recently, and prunes
// each aircraft's position history to keep only fresh positions.
fn prune_stale_aircraft(
    history: &mut AppState,
    now: DateTime<Utc>,
    max_history_age: Duration,
) -> usize {
    let num_before = history.0.len();
    // Prune aircraft that we haven't seen in a while, unless they recently
    // stopped circling.
    //
    // First we filter out any position reports that are older than
    // max_history_age.
    history.0.values_mut().for_each(|ac| {
        ac.history.retain(|pos| (now - pos.time) < max_history_age);
    });
    // Then we filter out any aircraft that no longer have any position reports,
    // _unless_ they stopped circling less than 30 minutes ago. This helps us
    // follow our rule of not tweeting an aircraft again unless it takes at
    // least a 30 minute break from circling.
    history.0.retain(|_hex, ac| {
        !ac.history.is_empty()
            || ac
                .ended_circling_time
                .map_or(false, |t| now - t < Duration::minutes(30))
    });
    num_before - history.0.len()
}

fn get_config(config_path: &str, secrets_path: &str) -> anyhow::Result<AppConfig> {
    let mut config = read_config(config_path)?;
    let secrets = read_secrets(secrets_path)?;
    // Merge credentials into the config.
    config.adsbx.creds = secrets.adsbx;
    config.twitter.creds = secrets.twitter;
    Ok(config)
}

fn read_history_db(path: &str) -> anyhow::Result<AppState> {
    debug!("Loading history from {}", path);
    let history =
        AppState::from_file(path).with_context(|| format!("Reading history file from {}", path))?;
    debug!("Read {} aircraft from history", history.0.len());
    Ok(history)
}

async fn get_adsbx_data(config: &AppConfig) -> anyhow::Result<adsbx_json::v2::Response> {
    let adsbx_api = AdsbxApi::from_config(config);
    let adsbx_data = match (config.lat, config.lon) {
        (Some(lat), Some(lon)) => {
            adsbx_api
                .get_nearby(lat, lon, config.radius_km.unwrap() * 0.539957)
                .await
        }
        (None, None) => adsbx_api.get_all().await,
        _ => Err(Error::ConfigError(
            "Both lat and lon must be specified if either one is specified".to_string(),
        )),
    };
    if let Ok(adsbx_data) = &adsbx_data {
        debug!("Got {} aircraft from ADSBX", adsbx_data.aircraft.len());
    }
    adsbx_data.context("Fetching and parsing ADS-B Exchange API data")
}

fn remove_blocked_icaos(adsbx_data: &mut adsbx_json::v2::Response, config: &AppConfig) {
    let start_len = adsbx_data.aircraft.len();
    if let Some(ref icao_blocklist) = config.icao_blocklist {
        let icao_blocklist: Vec<String> = icao_blocklist.iter().map(|s| s.to_lowercase()).collect();
        adsbx_data
            .aircraft
            .retain(|ac| !icao_blocklist.contains(&ac.hex));
    }
    let end_len = adsbx_data.aircraft.len();
    debug!(
        "Filtered out {} aircraft due to blocklist, leaving {}",
        start_len - end_len,
        end_len
    );
}

fn keep_specified_icaos(adsbx_data: &mut adsbx_json::v2::Response, config: &AppConfig) {
    let start_len = adsbx_data.aircraft.len();
    if let Some(ref icao_allowlist) = config.icaos {
        let icao_allowlist: Vec<String> = icao_allowlist.iter().map(|s| s.to_lowercase()).collect();
        adsbx_data
            .aircraft
            .retain(|ac| icao_allowlist.contains(&ac.hex));
    }
    let end_len = adsbx_data.aircraft.len();
    debug!(
        "Filtered out {} aircraft due to allowlist, leaving {}",
        start_len - end_len,
        end_len
    );
}

fn update_history(
    history: &mut AppState,
    adsbx_data: &adsbx_json::v2::Response,
    now: DateTime<Utc>,
    config: &AppConfig,
) {
    add_new_data(history, adsbx_data, now);
    for ac in history.0.values_mut() {
        ac.update_curviness();
    }
    prune_stale_aircraft(
        history,
        now,
        chrono::Duration::from_std(config.max_history_age).unwrap(),
    );
}

fn add_new_data(history: &mut AppState, adsbx_data: &adsbx_json::v2::Response, now: DateTime<Utc>) {
    let mut num_updated = 0;
    let mut num_new = 0;
    for ac in &adsbx_data.aircraft {
        match history.0.get_mut(&ac.hex) {
            Some(hist_ac) => {
                hist_ac.update_from_aircraft(ac);
                num_updated += 1;
            }
            None => {
                if let Ok(ac_state) = AircraftState::from_aircraft(ac, now) {
                    history.0.insert(ac.hex.clone(), ac_state);
                    num_new += 1;
                }
            }
        }
    }
    debug!("Updated {} aircraft, added {} new", num_updated, num_new);
}

fn detect_circles<'a>(
    history: &'a mut AppState,
    now: DateTime<Utc>,
    config: &AppConfig,
) -> Vec<&'a AircraftState> {
    let mut circles: Vec<&AircraftState> = Vec::new();
    if let Some(hex) = &config.simulate_circling_aircraft {
        if let Some(ac) = history.0.get_mut(hex) {
            circles.push(ac);
        }
    } else {
        for ac in history.0.values_mut() {
            let is_circling = ac.is_circling(MIN_CURVINESS);
            let was_previously_circling = ac.was_circling(&now);
            if is_circling
                && !was_previously_circling
                && ac
                    .ended_circling_time
                    .map_or(true, |t| now - t > Duration::minutes(30))
            {
                ac.started_circling_time = Some(now);
                ac.ended_circling_time = None;
                circles.push(ac);
            } else if !is_circling && was_previously_circling {
                info!(
                    "{}: Circle terminated after {} secs",
                    ac.icao,
                    (now - ac.started_circling_time.unwrap()).num_seconds()
                );
                ac.started_circling_time = None;
                ac.ended_circling_time = Some(now);
            }
        }
    }
    circles
}

async fn closest_airport(
    point: Point<f64>,
    config: &AppConfig,
) -> Result<Option<pelias::Feature>, Error> {
    let blocklist_regexes = config
        .airport
        .as_ref()
        .map(|a| a.blocklist_regexes.clone())
        .unwrap_or_default();
    let pelias = PeliasApi::from_url(&config.pelias.url)?;
    let airports = pelias
        .nearby(&NearbyOptions {
            point: Some(point),
            categories: Some("transport:air:aerodrome"),
            boundary: Some(Boundary::Circle(7.0)),
            ..NearbyOptions::default()
        })
        .await?
        .features;
    Ok(airports
        .into_iter()
        .filter(|f| {
            // Filter out airports that are in the blocklist.
            !blocklist_regexes
                .iter()
                .any(|re| re.is_match(&f.properties.name))
        })
        .sorted_by(|a, b| {
            (a.properties.distance)
                .partial_cmp(&(b.properties.distance))
                .unwrap()
        })
        .next())
}

async fn closest_landmark(
    point: Point<f64>,
    config: &AppConfig,
) -> Result<Option<pelias::Feature>, Error> {
    let blocklist_regexes = config
        .landmarks
        .as_ref()
        .map(|a| a.blocklist_regexes.clone())
        .unwrap_or_default();
    let pelias = PeliasApi::from_url(&config.pelias.url)?;
    let mut landmarks = pelias
        .nearby(&NearbyOptions {
            point: Some(point),
            layers: Some("venue"),
            size: Some(50),
            boundary: Some(Boundary::Circle(100.0)),
            ..NearbyOptions::default()
        })
        .await?
        .features;
    landmarks = landmarks
        .into_iter()
        .filter(|f| {
            // Filter out airports that are in the blocklist.
            !blocklist_regexes
                .iter()
                .any(|re| re.is_match(&f.properties.name))
        })
        .sorted_by(|a, b| {
            (a.properties.distance)
                .partial_cmp(&(b.properties.distance))
                .unwrap()
        })
        .collect();
    debug!("Nearest landmarks:");
    for l in landmarks.iter().take(3) {
        debug!(
            "{:5.2} {:55} {}",
            &l.properties.distance, &l.properties.label, &l.properties.gid
        );
    }
    Ok(landmarks.into_iter().next())
}

async fn reverse_geo(
    point: Point<f64>,
    config: &AppConfig,
) -> anyhow::Result<Option<pelias::Feature>> {
    let pelias = PeliasApi::from_url(&config.pelias.url)?;
    let reverse_geo = pelias
        .reverse_geo(&NearbyOptions {
            point: Some(point),
            layers: Some("coarse"),
            ..NearbyOptions::default()
        })
        .await?
        .features;
    Ok(reverse_geo.into_iter().next())
}

const MAX_WILDFIRE_DISTANCE_METERS: f64 = 10000.0;

async fn process_potential_circles(
    circles: &[&AircraftState],
    tweet_grammar: &genex::Grammar,
    now: DateTime<Utc>,
    config: &AppConfig,
) -> anyhow::Result<()> {
    for ac in circles {
        info!(
            "{}: Circle detected. curviness: {:6.0} registration: {:9} {} {:?}",
            ac.icao,
            ac.curviness,
            ac.registration.as_ref().unwrap_or(&"<no reg>".to_string()),
            ac.adsbx_url(),
            ac.started_circling_time,
        );
        match ac.recent_centroid(now) {
            Some(centroid) => {
                info!(
                    "{}: Centroid: https://maps.google.com/?q={},{}",
                    ac.icao,
                    centroid.y(),
                    centroid.x()
                );
                let mut grammar_data = HashMap::new();
                // Check if the centroid is near a known wildfire.
                let nearby_wildfires = fires::nearby_fires(&centroid, 1000.0 * 100.0).await;
                match nearby_wildfires {
                    Ok(wildfires) => {
                        for wildfire in &wildfires {
                            info!(
                                "{}: Wildfire {:?} is {:.1} km away",
                                ac.icao,
                                wildfire.name,
                                wildfire.distance_m / 1000.0,
                            );
                        }
                        if !wildfires.is_empty() {
                            let wildfire = &wildfires[0];
                            if wildfire.distance_m < MAX_WILDFIRE_DISTANCE_METERS {
                                add_wildfire_data(&mut grammar_data, wildfire);
                            }
                        }
                    }
                    Err(e) => {
                        error!("Error getting nearby wildfires; {}", e);
                    }
                }
                // Check to see if the centroid is too close to an airport. If
                // it's too close, we assume it's pattern work and we filter it
                // out.
                let airport = closest_airport(centroid, config).await?;
                match &airport {
                    Some(airport) => {
                        info!(
                            "{}: Closest airport is {}, distance {:.1} km",
                            ac.icao, airport.properties.label, airport.properties.distance
                        );
                    }
                    None => {
                        info!("{}: No airports nearby", ac.icao);
                    }
                }
                if let Some(airport) = airport {
                    if airport.properties.distance <= config.min_airport_distance_km {
                        info!(
                            "{}: Filtering out because it's {} km (minimum is {}) from {}",
                            ac.icao,
                            airport.properties.distance,
                            config.min_airport_distance_km,
                            airport.properties.label
                        );
                        break;
                    }
                }
                // Get nearby landmarks.
                let reverse_geo = reverse_geo(centroid, config).await?;
                let landmark = closest_landmark(centroid, config).await?;
                let record = aircraft_db::get_record(
                    &ac.icao,
                    config.aircraft_info_db.as_ref().ok_or_else(|| {
                        Error::ConfigError("How is aircraft_info_db not set?".to_string())
                    })?,
                )?;
                info!("Record: {:?}", &record);
                add_aircraft_data(&mut grammar_data, ac, &record);
                if let Some(reverse_geo) = reverse_geo {
                    add_reverse_geo_data(&mut grammar_data, &reverse_geo);
                }
                if let Some(landmark) = landmark {
                    add_landmark_data(&mut grammar_data, &landmark);
                }
                debug!("{:#?}", grammar_data);
                let tweet_text =
                    tweet_grammar
                        .generate("top", &grammar_data)?
                        .ok_or_else(|| {
                            Error::TweetError(
                                "Could not generate tweet text for some reason".to_string(),
                            )
                        })?;
                warn!("{}", tweet_text);
                if config.simulate_circling_aircraft.is_some() {
                    return Ok(());
                }
                let mut images = vec![];
                let screenshot = screenshot(&ac.icao, config)?;
                let mut file = File::create(format!(
                    "{}.png",
                    base_filename(&config.log_prefix, &ac.icao, now)
                ))?;
                file.write_all(&screenshot.data)?;
                images.push(screenshot.data);
                match get_photo(&ac.icao).await {
                    Ok(photo_opt) => match photo_opt {
                        Some(photo) => {
                            images.push(photo);
                        }
                        None => {
                            info!("No photo for {}", ac.icao);
                        }
                    },
                    // Don't die if airport-data.com is down.
                    Err(err) => {
                        warn!("Error getting photo for {}: {}", ac.icao, err);
                    }
                }
                let geojson = track_to_geojson(&ac.icao, &ac.history, centroid).to_string();
                let geojson_path = format!(
                    "{}.geojson",
                    base_filename(&config.log_prefix, &ac.icao, now)
                );
                let mut geojson_file = std::fs::File::create(geojson_path)?;
                geojson_file.write_all(geojson.as_bytes())?;
                if config.twitter.is_enabled {
                    tweet(&tweet_text, images, centroid, config).await?;
                }
            }
            None => {
                warn!("{}: No recent centroid", ac.icao);
            }
        }
    }
    Ok(())
}

fn add_landmark_data(data: &mut HashMap<String, String>, landmark: &pelias::Feature) {
    data.insert(
        "landmark_name".to_string(),
        landmark.properties.name.clone(),
    );
    data.insert(
        "landmark_distance".to_string(),
        format!("{:.1}", landmark.properties.distance),
    );
}

fn add_wildfire_data(data: &mut HashMap<String, String>, wildfire: &fires::SituatedFire) {
    data.insert("fire_name".to_string(), wildfire.name.clone());
    data.insert(
        "fire_distance".to_string(),
        format!("{:.1}", wildfire.distance_m / 1609.34),
    );
}

fn add_aircraft_data(
    data: &mut HashMap<String, String>,
    ac: &AircraftState,
    record: &Option<aircraft_db::Record>,
) {
    data.insert("icao".to_string(), ac.icao.clone());
    let registration = ac
        .registration
        .as_ref()
        .or_else(|| record.as_ref().and_then(|r| r.registration.as_ref()));
    if let Some(reg) = registration {
        data.insert("registration".to_string(), reg.to_string());
    }
    if let Some(ac_type) = record.as_ref().and_then(|r| r.ac_type.as_ref()) {
        data.insert("type".to_string(), ac_type.clone());
    }
    if let Some(speed) = ac.speed {
        data.insert("speed".to_string(), format!("{:.0}", speed));
    }
    if let Some(alt) = ac.geom_alt.or(ac.alt) {
        data.insert("alt".to_string(), format!("{:.0}", alt));
    }
    if let Some(squawk) = &ac.squawk {
        data.insert("squawk".to_string(), squawk.to_string());
    }
    if let Some(call_sign) = &ac.call_sign.as_ref().map(|cs| cs.trim()) {
        if registration.is_none() || registration.unwrap() != call_sign {
            data.insert("call_sign".to_string(), call_sign.to_string());
        }
    }
}

fn add_reverse_geo_data(data: &mut HashMap<String, String>, reverse_geo: &pelias::Feature) {
    if let Some(neighbourhood) = &reverse_geo.properties.neighbourhood {
        data.insert("neighbourhood".to_string(), neighbourhood.clone());
    }
    if let Some(localadmin) = &reverse_geo.properties.localadmin {
        data.insert("localadmin".to_string(), localadmin.clone());
    }
    if let Some(locality) = &reverse_geo.properties.locality {
        data.insert("locality".to_string(), locality.clone());
    }
    if let Some(county) = &reverse_geo.properties.county {
        data.insert("county".to_string(), county.clone());
    }
    data.insert("name".to_string(), reverse_geo.properties.name.clone());
}

fn update_config_from_args(config: &mut AppConfig, args: &CliArgs) {
    if args.log_prefix.is_some() {
        config.log_prefix = args.log_prefix.clone();
    }
    config.aircraft_info_db = Some(args.aircraft_info_db.clone());
    config.simulate_circling_aircraft = args.simulate.clone();
}

#[tokio::main]
async fn main() -> anyhow::Result<()> {
    let start = Instant::now();
    let args = CliArgs::from_args();
    setup_logger(&args.log_prefix)?;

    // Handle --create-aircraft-info-db <json path>.
    if let Some(json_path) = args.create_aircraft_info_db_from_json {
        aircraft_db::create_db_from_json_file(&json_path, &args.aircraft_info_db)?;
        return Ok(());
    }

    // Handle --lookup-aircraft <icao>.
    if let Some(icao) = args.lookup_aircraft {
        let record = aircraft_db::get_record(&icao, &args.aircraft_info_db)?;
        println!("{:#?}", record);
        return Ok(());
    }

    // Handle --get-twitter-auth-token.
    if args.get_twitter_auth_token {
        interactively_get_oauth_credentials().await?;
        return Ok(());
    }

    let now = Utc::now();
    let mut config = get_config(&args.config, &args.secrets)?;
    update_config_from_args(&mut config, &args);

    let tweet_grammar = genex::Grammar::from_str(include_str!("tweet.genx"))?;
    let mut history = read_history_db(&args.history)?;
    let mut adsbx_data = get_adsbx_data(&config).await?;
    // Update state.
    remove_blocked_icaos(&mut adsbx_data, &config);
    keep_specified_icaos(&mut adsbx_data, &config);
    if config.simulate_circling_aircraft.is_none() {
        update_history(&mut history, &adsbx_data, now, &config);
    }
    let potential_circles = detect_circles(&mut history, now, &config);
    process_potential_circles(&potential_circles, &tweet_grammar, now, &config).await?;

    let most_circly: Vec<&AircraftState> = history
        .0
        .values()
        .filter(|ac| !ac.is_circling(MIN_CURVINESS))
        .sorted_by(|a, b| a.curviness.abs().partial_cmp(&b.curviness.abs()).unwrap())
        .rev()
        .take(3)
        .collect();
    let circling: Vec<&AircraftState> = history
        .0
        .values()
        .filter(|ac| ac.is_circling(MIN_CURVINESS) && ac.started_circling_time.is_some())
        .collect();
    debug!("Currently tracking {} circling aircraft:", circling.len());
    for a in circling {
        debug!(
            "{:6.0}: {:7} {:9} {:4} minutes",
            a.curviness,
            a.icao,
            a.registration.as_ref().unwrap_or(&"".to_string()),
            (now - a.started_circling_time.unwrap()).num_minutes(),
        );
    }
    let most_circly_string = most_circly
        .iter()
        .map(|a| format!("{:>7}:{:<5.0}", &a.icao, a.curviness))
        .join(" ");
    if config.simulate_circling_aircraft.is_none() {
        history
            .save_file(&args.history)
            .with_context(|| format!("Saving history file to {}", &args.history))?;
    }
    info!(
        "Tracking {:4} aircraft in {:5} ms. Most circly: {}",
        history.0.len(),
        start.elapsed().as_millis(),
        most_circly_string
    );
    Ok(())
}
