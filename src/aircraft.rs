use crate::errors::Error;
use adsbx_json::v2::{Aircraft, AltitudeOrGround};
use chrono::serde::ts_milliseconds;
use chrono::{prelude::*, Duration};
use geo::{
    point,
    prelude::{Bearing, Centroid},
    MultiPoint, Point,
};
use geojson::GeoJson;
use itertools::Itertools;
use log::info;
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Debug, PartialEq, Clone)]
#[serde(deny_unknown_fields)]
pub struct AircraftState {
    pub alt: Option<i32>,
    #[serde(rename = "callsign")]
    pub call_sign: Option<String>,
    pub curviness: f64,
    pub icao: String,
    #[serde(rename = "geom-alt")]
    pub geom_alt: Option<i32>,
    pub history: Vec<Pos>,
    #[serde(rename = "military?")]
    pub is_military: bool,
    #[serde(rename = "gnd?")]
    pub is_on_gnd: bool,
    pub lat: f64,
    pub lon: f64,
    #[serde(rename = "normalized-curviness")]
    pub normalized_curviness: Option<f64>,
    pub squawk: Option<String>,
    pub registration: Option<String>,
    pub postime: DateTime<Utc>,
    #[serde(rename = "started-circling-time")]
    #[serde(default)]
    pub started_circling_time: Option<DateTime<Utc>>,
    #[serde(rename = "ended-circling-time")]
    #[serde(default)]
    pub ended_circling_time: Option<DateTime<Utc>>,
    pub speed: Option<f64>,
}

impl Default for AircraftState {
    fn default() -> Self {
        AircraftState {
            alt: None,
            call_sign: None,
            curviness: 0.0,
            icao: "000000".to_string(),
            geom_alt: None,
            history: vec![],
            is_military: false,
            is_on_gnd: false,
            lat: 0.0,
            lon: 0.0,
            normalized_curviness: None,
            squawk: None,
            registration: None,
            postime: Utc::now(),
            started_circling_time: None,
            ended_circling_time: None,
            speed: None,
        }
    }
}

impl AircraftState {
    pub fn from_aircraft(ac: &Aircraft, now: DateTime<Utc>) -> Result<AircraftState, Error> {
        let (lat, lon) = match (ac.lat, ac.lon) {
            (Some(lat), Some(lon)) => (lat, lon),
            _ => {
                return Err(Error::AircraftStateError(
                    "aircraft must have lat and lon".to_string(),
                ))
            }
        };
        let seen_pos = match ac.seen_pos {
            Some(seen_pos) => seen_pos,
            None => {
                return Err(Error::AircraftStateError(
                    "aircraft must have seen_pos".to_string(),
                ))
            }
        };
        let postime = now
            .checked_sub_signed(chrono::Duration::from_std(seen_pos).map_err(|e| {
                Error::AircraftStateError(format!("seen_pos {:?} is out of range: {}", seen_pos, e))
            })?)
            .ok_or_else(|| {
                Error::AircraftStateError(format!(
                    "seen_pos {:?} and {:?} now caused overflow",
                    seen_pos, now,
                ))
            })?;
        Ok(AircraftState {
            icao: ac.hex.clone(),
            is_military: ac.database_flags.is_military(),
            alt: ac.geometric_altitude,
            speed: ac.ground_speed_knots,
            normalized_curviness: None,
            curviness: 0.0,
            squawk: ac.squawk.clone(),
            geom_alt: ac.geometric_altitude,
            history: vec![Pos {
                alt: ac.geometric_altitude,
                is_on_gnd: false,
                lat,
                lon,
                time: postime,
            }],
            is_on_gnd: false,
            call_sign: ac.call_sign.clone(),
            lat,
            lon,
            registration: ac.registration.clone(),
            postime,
            started_circling_time: None,
            ended_circling_time: None,
        })
    }

    pub fn update_curviness(&mut self) {
        self.curviness = curviness(&self.history);
    }

    pub fn is_circling(&self, min_curviness: f64) -> bool {
        self.curviness.abs() > min_curviness
    }

    pub fn was_circling(&self, now: &DateTime<Utc>) -> bool {
        match (&self.started_circling_time, &self.ended_circling_time) {
            (None, _) => false,
            (Some(start_time), None) => start_time < now,
            (Some(start_time), Some(end_time)) => start_time < now && end_time >= now,
        }
    }

    pub fn update_from_aircraft(&mut self, ac: &Aircraft) {
        match ac.barometric_altitude {
            Some(AltitudeOrGround::Altitude(alt)) => {
                self.alt = Some(alt);
            }
            Some(AltitudeOrGround::OnGround) => {
                self.alt = Some(0);
            }
            None => {}
        }
        if let Some(call_sign) = &ac.call_sign {
            self.call_sign = Some(call_sign.to_string());
        }
        if let Some(geom_alt) = ac.geometric_altitude {
            self.geom_alt = Some(geom_alt);
        }
        if let Some(pos) = Pos::from_aircraft(ac, Utc::now()) {
            self.history.push(pos);
        }
        if let Some(gs_knots) = ac.ground_speed_knots {
            self.speed = Some(gs_knots);
        }
    }

    pub fn most_recent_circle_start_idx(&self, threshold: f64) -> Option<usize> {
        let fool = self
            .history
            .iter()
            .enumerate()
            .rev()
            .filter(|(_, p)| !p.is_on_gnd)
            .tuple_windows()
            //.inspect(|((i, p1), (j, p2))| println!("positions {},{:?}  {},{:?}", i, p1, j, p2))
            .map(|((_i, p1), (j, p2))| (j, p2.bearing(p1)))
            .tuple_windows()
            .map(|((_i, b1), (j, b2))| (j, bearing_difference(b2, b1)))
            //.inspect(|(i, b)| println!("bearing deltas {} {}", i, b))
            .scan(0.0, |sum, (ref i, ref delta)| {
                *sum += *delta; //delta
                Some((*i, *sum))
            })
            //.inspect(|(i, sum)| println!("sums {} {}", i, sum))
            .find(|(_i, sum)| (*sum).abs() > threshold);
        //println!("FOOL: {:?}", fool);
        fool.map(|f| f.0)
    }

    pub fn recent_centroid2(&self) -> Option<Point<f64>> {
        if let Some(idx) = self.most_recent_circle_start_idx(720.0) {
            let points: MultiPoint<_> = self
                .history
                .iter()
                .enumerate()
                .rev()
                .take_while(|(i, _p)| *i >= idx)
                .map(|(_i, p)| (p.lon, p.lat))
                .collect::<Vec<(f64, f64)>>()
                .into();
            points.centroid()
        } else {
            None
        }
    }

    pub fn recent_centroid(&self, now: DateTime<Utc>) -> Option<Point<f64>> {
        let recent_history_cutoff = Duration::minutes(5);
        let points: MultiPoint<_> = self
            .history
            .iter()
            .filter(|pos| pos.time > (now - recent_history_cutoff))
            .map(|pos| (pos.lon, pos.lat))
            .collect::<Vec<(f64, f64)>>()
            .into();
        info!("centroid points: {:?}", points);
        match self.history.last() {
            Some(pos) => {
                info!("Most recent position is {} seconds old", now - pos.time);
            }
            None => todo!(),
        }
        points.centroid()
    }

    pub fn adsbx_url(&self) -> String {
        format!("https://globe.adsbexchange.com/?icao={}", self.icao)
    }
}

pub fn curviness(positions: &[Pos]) -> f64 {
    positions
        .iter()
        .filter(|p| p.alt.unwrap_or(0) > 0)
        // Now we have points where the aircraft isn't on the ground.
        .tuple_windows()
        .map(|(p1, p2)| p1.bearing(p2))
        // Now we have bearings.
        .tuple_windows()
        .map(|(b1, b2)| bearing_difference(b1, b2))
        // Now we have bearing deltas, which range from -180 to 180.
        .sum()
}

// Computes the difference between two bearings, in the range -180 to 180.
pub fn bearing_difference(a: f64, b: f64) -> f64 {
    normalize_bearing_180(a - b)
}

/// Normalizes a bearing into the range -180 to 180.
pub fn normalize_bearing_180(b: f64) -> f64 {
    if b < -180.0 {
        normalize_bearing_180(b + 360.0)
    } else if b > 180.0 {
        normalize_bearing_180(b - 360.0)
    } else {
        b
    }
}

// Returns altitude, with OnGround being equivalent to 0.
pub fn alt_from_barometric_alt(balt: &Option<AltitudeOrGround>) -> Option<i32> {
    match balt {
        Some(AltitudeOrGround::Altitude(alt)) => Some(*alt),
        Some(AltitudeOrGround::OnGround) => Some(0),
        None => None,
    }
}

#[derive(Serialize, Deserialize, Debug, PartialEq, Clone)]
pub struct Pos {
    pub alt: Option<i32>,
    #[serde(rename = "gnd?")]
    pub is_on_gnd: bool,
    pub lat: f64,
    pub lon: f64,
    #[serde(with = "ts_milliseconds")]
    pub time: DateTime<Utc>,
}

impl Pos {
    /// Creates a Pos from an ADS-B Exchange JSON Aircraft data structure.
    pub fn from_aircraft(ac: &Aircraft, now: DateTime<Utc>) -> Option<Pos> {
        let postime = match ac
            .seen_pos
            .and_then(|seen_pos| Duration::from_std(seen_pos).ok())
            .map(|d| now - d)
        {
            Some(dt) => dt,
            None => return None,
        };
        if let (Some(lat), Some(lon)) = (ac.lat, ac.lon) {
            Some(Pos {
                alt: alt_from_barometric_alt(&ac.barometric_altitude),
                is_on_gnd: ac.barometric_altitude == Some(AltitudeOrGround::OnGround),
                lat,
                lon,
                time: postime,
            })
        } else {
            None
        }
    }

    /// Computes the bearing, in degrees, from one Pos to another.
    pub fn bearing(&self, pos: &Pos) -> f64 {
        let a = point!(x: self.lon, y: self.lat);
        let b = point!(x: pos.lon, y: pos.lat);
        a.bearing(b)
    }

    /// Parses a GeoJSON string into an collection of Pos. For debugging and
    /// testing.
    pub fn from_geojson_str(geojson_str: &str) -> Result<Vec<Pos>, Error> {
        let mut positions = Vec::new();
        let geoson = geojson_str
            .parse::<GeoJson>()
            .map_err(|e| Error::GeoJsonError(format!("Error parsing GeoJSON:{}", e)))?;
        match geoson {
            GeoJson::Geometry(ref geom) => {
                positions.extend(geom_to_pos(geom)?);
            }
            GeoJson::Feature(ref feat) => {
                if let Some(ref geom) = feat.geometry {
                    positions.extend(geom_to_pos(geom)?);
                }
            }
            GeoJson::FeatureCollection(ref coll) => {
                for feature in &coll.features {
                    if let Some(ref geom) = feature.geometry {
                        positions.extend(geom_to_pos(geom)?);
                    }
                }
            }
        }
        Ok(positions)
    }
}

fn geom_to_pos(geom: &geojson::Geometry) -> Result<Vec<Pos>, Error> {
    match &geom.value {
        geojson::Value::Point(point) => Ok(vec![Pos {
            lon: point[0],
            lat: point[1],
            alt: point.get(2).map(|alt| alt.round() as i32),
            is_on_gnd: false,
            time: chrono::Utc::now(),
        }]),
        geojson::Value::MultiPoint(points) => Ok(points
            .iter()
            .map(|p| Pos {
                lon: p[0],
                lat: p[1],
                alt: p.get(2).map(|alt| alt.round() as i32),
                is_on_gnd: false,
                time: chrono::Utc::now(),
            })
            .collect()),
        geojson::Value::LineString(points) => Ok(points
            .iter()
            .map(|p| Pos {
                lon: p[0],
                lat: p[1],
                alt: p.get(2).map(|alt| alt.round() as i32),
                is_on_gnd: false,
                time: chrono::Utc::now(),
            })
            .collect()),
        geojson::Value::MultiLineString(lines) => Ok(lines
            .iter()
            .flat_map(|points| {
                points
                    .iter()
                    .map(|p| Pos {
                        lon: p[0],
                        lat: p[1],
                        alt: p.get(2).map(|alt| alt.round() as i32),
                        is_on_gnd: false,
                        time: chrono::Utc::now(),
                    })
                    .collect::<Vec<Pos>>()
            })
            .collect()),
        geojson::Value::Polygon(_) => Err(Error::GeoJsonError(
            "Cannot read positions from geojson polygon geometry".to_string(),
        )),
        geojson::Value::MultiPolygon(_) => Err(Error::GeoJsonError(
            "Cannot read positions from geojson multipolygon geometry".to_string(),
        )),
        geojson::Value::GeometryCollection(geometries) => {
            let mut positions = Vec::new();
            for r in geometries.iter().map(geom_to_pos) {
                positions.extend(r?);
            }
            Ok(positions)
        }
    }
}

#[cfg(test)]
mod tests {
    // Note this useful idiom: importing names from outer (for mod tests) scope.
    use super::*;

    #[test]
    fn test_is_was_circling() {
        let mut ac = AircraftState {
            curviness: 5000.0,
            started_circling_time: None,
            ended_circling_time: None,
            ..AircraftState::default()
        };
        assert!(ac.is_circling(1000.0));
        assert!(!ac.was_circling(&Utc::now()));
        let now = Utc::now();
        ac = AircraftState {
            curviness: 5000.0,
            started_circling_time: Some(now),
            ended_circling_time: None,
            ..AircraftState::default()
        };
        assert!(ac.was_circling(&(now + Duration::minutes(1))));
        ac = AircraftState {
            curviness: 5000.0,
            started_circling_time: Some(now),
            ended_circling_time: Some(now + Duration::minutes(1)),
            ..AircraftState::default()
        };
        assert!(!ac.was_circling(&(now + Duration::minutes(2))));
    }
}
