use std::time::Instant;

use crate::{errors::Error, USER_AGENT};
use geo::Point;
use log::{debug, trace};
use reqwest::Url;
use serde::Deserialize;

pub struct PeliasApi {
    url: Url,
}

impl PeliasApi {
    pub fn from_url(url: &str) -> Result<PeliasApi, Error> {
        Ok(PeliasApi {
            url: Url::parse(url).map_err(|e| {
                Error::PeliasError(format!("Error parsing pelias URL {}: {}", url, e))
            })?,
        })
    }

    pub async fn nearby(&self, options: &NearbyOptions<'_>) -> Result<Response, Error> {
        let client = reqwest::Client::builder()
            .user_agent(USER_AGENT)
            .build()
            .map_err(|e| Error::PeliasError(format!("Error building pelias http client: {}", e)))?;
        let url = self.url.join("/v1/nearby").map_err(|e| {
            Error::PeliasError(format!(
                "Error parsing pelias URL {}/v1/nearby: {}",
                self.url, e
            ))
        })?;
        let params = options.to_params();
        debug!(
            "Performing nearby pelias query url: {} params: {:?}",
            url.as_str(),
            params
        );
        let start = Instant::now();
        let resp = client.get(url.clone()).query(&params).send().await;
        debug!("Got pelias response in {} ms", start.elapsed().as_millis());
        let body = resp
            .map_err(|e| {
                Error::PeliasError(format!(
                    "Error getting pelias response from {}: {}",
                    &url, e
                ))
            })?
            .error_for_status()
            .map_err(|e| {
                Error::PeliasError(format!(
                    "Pelias returned an error status for {}: {}",
                    &url, e
                ))
            })?
            .text()
            .await
            .map_err(|e| {
                Error::PeliasError(format!("Error getting pelias response body: {}", e))
            })?;
        let resp: Response = serde_json::from_str(&body).map_err(|e| {
            Error::PeliasError(format!(
                "Error parsing pelias response from {}: {}",
                &url, e
            ))
        })?;
        trace!("Pelias response: {:?}", resp);
        Ok(resp)
    }

    pub async fn reverse_geo(&self, options: &NearbyOptions<'_>) -> Result<Response, Error> {
        let client = reqwest::Client::builder()
            .user_agent(USER_AGENT)
            .build()
            .map_err(|e| Error::PeliasError(format!("Error building pelias http client: {}", e)))?;
        let url = self.url.join("/v1/reverse").map_err(|e| {
            Error::PeliasError(format!(
                "Error parsing pelias url {}/v1/reverse: {}",
                &self.url, e
            ))
        })?;
        let params = options.to_params();
        debug!(
            "Performing reverse pelias query url: {} params: {:?}",
            url.as_str(),
            params
        );
        let start = Instant::now();
        let resp = client.get(url.clone()).query(&params).send().await;
        debug!("Got pelias response in {} ms", start.elapsed().as_millis());
        let body = resp
            .map_err(|e| {
                Error::PeliasError(format!(
                    "Error getting pelias response from {}: {}",
                    &url, e
                ))
            })?
            .error_for_status()
            .map_err(|e| {
                Error::PeliasError(format!(
                    "Pelias returned an error status for {}: {}",
                    &url, e
                ))
            })?
            .text()
            .await
            .map_err(|e| {
                Error::PeliasError(format!("Error getting pelias response body: {}", e))
            })?;
        let resp: Response = serde_json::from_str(&body).map_err(|e| {
            Error::PeliasError(format!(
                "Error parsing pelias response from {}: {}",
                &url, e
            ))
        })?;
        trace!("Pelias response: {:?}", resp);
        Ok(resp)
    }
}

pub struct Circle {
    pub lat: f64,
    pub lon: f64,
    pub radius_km: f64,
}
pub enum Boundary {
    Circle(f64),
}

#[derive(Default)]
pub struct NearbyOptions<'a> {
    pub point: Option<Point<f64>>,
    pub size: Option<u32>,
    pub layers: Option<&'a str>,
    pub categories: Option<&'a str>,
    pub boundary: Option<Boundary>,
}

impl NearbyOptions<'_> {
    fn to_params(&self) -> Vec<(&str, String)> {
        let mut params = vec![];
        if let Some(point) = &self.point {
            params.push(("point.lat", point.y().to_string()));
            params.push(("point.lon", point.x().to_string()));
        }
        if let Some(boundary) = &self.boundary {
            match boundary {
                Boundary::Circle(radius) => {
                    params.push(("boundary.circle.radius", radius.to_string()))
                }
            }
        }
        if let Some(size) = self.size {
            params.push(("size", size.to_string()));
        }
        if let Some(layers) = &self.layers {
            params.push(("layers", layers.to_string()));
        }
        if let Some(categories) = &self.categories {
            params.push(("categories", categories.to_string()));
        }
        params
    }
}

#[derive(Deserialize, Debug, PartialEq, PartialOrd)]
pub struct Feature {
    pub properties: Properties,
}

#[derive(Deserialize, Debug, PartialEq, PartialOrd)]
pub struct Addendum {
    pub osm: Osm,
}

#[derive(Deserialize, Debug, PartialEq, PartialOrd)]
pub struct Osm {
    pub wikidata: Option<String>,
}

#[derive(Deserialize, Debug, PartialEq, PartialOrd)]
pub struct Properties {
    pub addendum: Option<Addendum>,
    pub country: Option<String>,
    pub county: Option<String>,
    pub distance: f64,
    pub gid: String,
    pub id: String,
    pub label: String,
    pub layer: String,
    pub locality: Option<String>,
    pub localadmin: Option<String>,
    pub name: String,
    pub neighbourhood: Option<String>,
    pub region: Option<String>,
    pub source: Option<String>,
}

#[derive(Deserialize, Debug, PartialEq, PartialOrd)]
pub struct Response {
    pub features: Vec<Feature>,
}
