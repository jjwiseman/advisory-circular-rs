use crate::errors::Error;
use geo::algorithm::euclidean_distance::EuclideanDistance;
use geojson::GeoJson;
use lazy_static::lazy_static;
use log::{error, info};
use proj::Transform;
use regex::Regex;

// Read the WFIGS - Current Wildland Fire Perimeters geojson file. See
// https://data-nifc.opendata.arcgis.com/datasets/nifc::wfigs-current-wildland-fire-perimeters/explore?location=33.750652%2C-108.478433%2C6.40
//
// Direct download link:
// https://opendata.arcgis.com/api/v3/datasets/2191f997056547bd9dc530ab9866ab61_0/downloads/data?format=geojson&spatialRefId=4326
//
// We run a caching reverse proxy to avoid hitting the ArcGIS server too often.

const WFIGS_GEOJSON_URL: &str = "http://lockheed.local/wildfires/api/v3/datasets/2191f997056547bd9dc530ab9866ab61_0/downloads/data?format=geojson&spatialRefId=4326";

/// Information about a named fire that is situated relative to some point.
#[derive(Debug, Clone, PartialEq, PartialOrd)]
pub struct SituatedFire {
    /// The distance, in meters, to the fire.
    pub distance_m: f64,
    /// The name of the fire incident.
    pub name: String,
}

// Fetch the WFGIS geojson file from the web.
pub async fn fetch_wfigs_geojson() -> Result<GeoJson, Error> {
    info!("Fetching wildfire data from {}", WFIGS_GEOJSON_URL);
    let response = reqwest::get(WFIGS_GEOJSON_URL).await;
    let body = response
        .map_err(|e| {
            Error::WildfireError(format!(
                "Error fetching WFIGS data from {}: {}",
                WFIGS_GEOJSON_URL, e
            ))
        })?
        .error_for_status()
        .map_err(|e| {
            Error::WildfireError(format!(
                "WFIGS returned an error status for {}: {}",
                WFIGS_GEOJSON_URL, e
            ))
        })?
        .text()
        .await
        .map_err(|e| Error::WildfireError(format!("Error getting WFIGS data body: {}", e)))?;
    let geojson = body
        .parse::<GeoJson>()
        .map_err(|e| Error::WildfireError(format!("Error parsing WFIGS geojson: {}", e)))?;
    Ok(geojson)
}

/// Returns the name of a fire incident based on available GeoJSON features. Also
/// does some cleanup of the name.
///
/// Examples of names this would return "COASTAL Fire", "Reservoir Fire 1".

fn fire_name(feature: &geojson::Feature) -> Option<String> {
    lazy_static! {
        static ref MULTI_WS_RE: Regex = Regex::new(r"\s+").expect("Error creating regex");
    }
    let incident_name = match feature.clone().properties {
        Some(properties) => properties
            // The name could be in either of two properties.
            .get("irwin_IncidentName")
            .or_else(|| properties.get("poly_IncidentName"))
            .and_then(|s| s.as_str())
            // They aren't very careful about extra spaces.
            .map(|s| MULTI_WS_RE.replace_all(s, " ").to_string())
            .map(|s| s.trim().to_string()),

        None => None,
    };
    // If "Fire" or "fire" is not already in the name, then add it.
    if let Some(name) = incident_name {
        if !name.contains("Fire") && !name.contains("fire") {
            Some(format!("{} Fire", name))
        } else {
            Some(name)
        }
    } else {
        incident_name
    }
}

/// Returns the distance from a point to a GeoJSON polygon in meters.

fn distance_to_feature(point: &geo::Point<f64>, feature: &geojson::Feature) -> f64 {
    let projection = {
        // GeoJSON uses the WGS 84 coordinate system
        let from = "EPSG:4326";
        // The NAD83 / California zone 6 (ftUS) coordinate system
        let to = "EPSG:2230";
        proj::Proj::new_known_crs(from, to, None).expect("Could not create projection")
    };
    if let Some(geometry) = &feature.geometry {
        let mut geom: geo::Geometry<f64> = match geometry.clone().value.try_into() {
            Ok(geom) => geom,
            Err(e) => {
                panic!("Error while converting geometry: {}", e);
            }
        };
        geom.transform(&projection)
            .expect("Could not transform geometry");
        let point = point
            .transformed(&projection)
            .expect("Could not transform point");
        let poly =
            geo::MultiPolygon::try_from(geom).expect("Could not convert geometry to polygon");
        poly.euclidean_distance(&point) * 0.3048
    } else {
        panic!("Feature has no geometry");
    }
}

/// Returns all known wildfires inside a circle defined by the specified point
/// and radius. The vector of wildfires is sorted by distance from the point.

pub async fn nearby_fires(
    point: &geo::Point<f64>,
    max_dist: f64,
) -> Result<Vec<SituatedFire>, Error> {
    let fire_geojson = fetch_wfigs_geojson().await?;
    let mut nearby_fires = Vec::new();
    match fire_geojson {
        GeoJson::Feature(feature) => {
            let distance_m = distance_to_feature(point, &feature);
            if distance_m < max_dist {
                if let Some(name) = fire_name(&feature) {
                    nearby_fires.push(SituatedFire { name, distance_m });
                } else {
                    error!("Fire has no name: {:?}", feature);
                }
            }
        }
        GeoJson::FeatureCollection(collection) => {
            for feature in collection.features {
                let distance_m = distance_to_feature(point, &feature);
                if distance_m < max_dist {
                    if let Some(name) = fire_name(&feature) {
                        nearby_fires.push(SituatedFire { name, distance_m });
                    } else {
                        error!("Fire has no name: {:?}", feature);
                    }
                }
            }
        }
        _ => {
            return Err(Error::WildfireError(format!(
                "Unexpected GeoJSON type: {:?}",
                fire_geojson
            )));
        }
    }
    nearby_fires.sort_by(|a, b| {
        a.distance_m
            .partial_cmp(&b.distance_m)
            .expect("Could not compare")
    });
    Ok(nearby_fires)
}

#[cfg(test)]
mod tests {
    // Note this useful idiom: importing names from outer (for mod tests) scope.
    use super::*;

    #[test]
    fn test_fire_name() {
        let feature = |s: &str| {
            let geojson: GeoJson = s.parse().expect("Could not parse geojson");
            match geojson {
                GeoJson::Feature(feature) => feature,
                _ => panic!("GeoJson not a feature"),
            }
        };
        let geojson_str = r#"
          {
            "type": "Feature",
            "geometry": {"type": "Point", "coordinates": [0, 0]},
            "properties": {
                "poly_IncidentName": "San Luis"
            }
          }
          "#;
        let fire = feature(geojson_str);
        let name = fire_name(&fire);
        assert_eq!(name, Some("San Luis Fire".to_string()));

        let geojson_str = r#"
          {
            "type": "Feature",
            "geometry": {"type": "Point", "coordinates": [0, 0]},
            "properties": {}
          }
          "#;
        let fire = feature(geojson_str);
        let name = fire_name(&fire);
        assert_eq!(name, None);

        let geojson_str = r#"
          {
            "type": "Feature",
            "geometry": {"type": "Point", "coordinates": [0, 0]},
            "properties": { "irwin_IncidentName": "San Fire Luis" }
          }
          "#;
        let fire = feature(geojson_str);
        let name = fire_name(&fire);
        assert_eq!(name, Some("San Fire Luis".to_string()));

        let geojson_str = r#"
          {
            "type": "Feature",
            "geometry": {"type": "Point", "coordinates": [0, 0]},
            "properties": { "irwin_IncidentName": "San  Fire Luis  " }
          }
          "#;
        let fire = feature(geojson_str);
        let name = fire_name(&fire);
        assert_eq!(name, Some("San Fire Luis".to_string()));
    }
}
