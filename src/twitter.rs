use crate::config::AppConfig;
use crate::errors::Error;
use egg_mode::media::{media_types, upload_media};
use egg_mode::tweet::DraftTweet;
use geo::Point;
use log::{debug, trace};

/// Posts a tweet.
pub async fn tweet(
    text: &str,
    images: Vec<Vec<u8>>,
    location: Point<f64>,
    config: &AppConfig,
) -> Result<(), Error> {
    debug!("Tweeting {:?} with {} images", text, images.len());
    if let Some(creds) = config.twitter.creds.as_ref() {
        let con_token = egg_mode::KeyPair::new(
            creds.consumer_key.to_string(),
            creds.consumer_secret.to_string(),
        );
        let access_token = egg_mode::KeyPair::new(
            creds.access_token.to_string(),
            creds.access_token_secret.to_string(),
        );
        let token = egg_mode::Token::Access {
            consumer: con_token,
            access: access_token,
        };

        let mut draft = DraftTweet::new(text.to_string());
        draft.coordinates = Some((location.y(), location.x()));
        draft.display_coordinates = Some(true);
        for image in images {
            let handle = upload_media(&image, &media_types::image_png(), &token)
                .await
                .map_err(|e| {
                    Error::TweetError(format!("Error uploading image to twitter: {}", e))
                })?;
            draft.add_media(handle.id);
        }
        let tweet = draft
            .send(&token)
            .await
            .map_err(|e| Error::TweetError(format!("Error sending tweet: {}", e)))?;
        trace!("Tweeted: {:#?}", tweet);
        Ok(())
    } else {
        Err(Error::TweetError(
            "No twitter credentials found in config".to_string(),
        ))
    }
}

/// Authenticates an Advisory Circular twitter app to be used with a specific
/// twitter account.
///
/// This function prompts the user to enter the app's consumer key and secret.
/// The user is then given a URL which they need to visit while logged into the
/// account they want to use for the bot, and they're prompted to enter the PIN
/// they're given at that URL.
///
/// At that point the generated access token and access token secret are
/// printed, which can be put in the secrets YAML file:
///
/// ```text
/// twitter:
///     consumer_key: <consumer key>
///     consumer_secret: <consumer secret>
///     access_token: <access token>
///     access_token_secret: <access token secret>
/// ```
pub async fn interactively_get_oauth_credentials() -> anyhow::Result<()> {
    let mut line = String::new();
    println!("Enter the bot app's consumer key:");
    std::io::stdin().read_line(&mut line)?;
    let consumer_key = line.trim().to_string();
    line.clear();
    println!("Enter the bot app's consumer secret:");
    std::io::stdin().read_line(&mut line)?;
    let consumer_secret = line.trim().to_string();
    line.clear();
    println!("consumer key: {:?}", consumer_key);
    println!("consumer secret: {:?}", consumer_secret);

    let con_token = egg_mode::KeyPair::new(consumer_key, consumer_secret);
    // "oob" is needed for PIN-based auth; see docs for `request_token` for more info
    let request_token = egg_mode::auth::request_token(&con_token, "oob").await?;

    let auth_url = egg_mode::auth::authorize_url(&request_token);
    println!("1. Open this URL and get the PIN: {}", auth_url);
    println!("2. Enter the PIN you got from the URL above: ");
    std::io::stdin().read_line(&mut line)?;
    let pin = line.trim().to_string();
    line.clear();
    let (token, user_id, screen_name) =
        egg_mode::auth::access_token(con_token, &request_token, pin).await?;
    println!("Authorizing account: {}", screen_name);
    println!("User ID: {}", user_id);
    println!("Token: {:?}", token);
    Ok(())
}
