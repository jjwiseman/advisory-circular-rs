#![warn(clippy::unwrap_used)]

use aircraft::Pos;
use chrono::prelude::*;
use geo::Point;
use geojson::Geometry;
pub mod adsbx;
pub mod aircraft;
pub mod aircraft_db;
pub mod airport_data;
pub mod app_state;
pub mod config;
pub mod errors;
pub mod fires;
pub mod pelias;
pub mod twitter;

const USER_AGENT: &str = "advisorycircular / 0.0";

pub fn file_timestamp_string(dt: DateTime<Utc>) -> String {
    dt.format("%Y%m%d-%H%M%S-%3f").to_string()
}

pub fn file_icao_string(icao: &str) -> String {
    if let Some(icao) = icao.strip_prefix('~') {
        icao.to_string().to_lowercase()
    } else {
        icao.to_string().to_lowercase()
    }
}

pub fn base_filename(log_prefix: &Option<String>, icao: &str, now: DateTime<Utc>) -> String {
    let mut filename = String::new();
    if let Some(prefix) = log_prefix {
        filename.push_str(&format!("{}-", prefix));
    }
    filename
        .push_str(format!("{}-{}", file_icao_string(icao), file_timestamp_string(now)).as_str());
    filename
}

pub fn track_to_geojson(
    icao: &str,
    positions: &[Pos],
    centroid: Point<f64>,
) -> geojson::FeatureCollection {
    let centroid_geometry = Geometry::new(geojson::Value::Point(vec![centroid.x(), centroid.y()]));
    let mut centroid_props = serde_json::Map::new();
    centroid_props.insert("marker-color".to_string(), "#7e7e7e".into());
    centroid_props.insert("marker-size".to_string(), "medium".into());
    centroid_props.insert("marker-symbol".to_string(), "".into());
    centroid_props.insert("icao".to_string(), icao.into());
    let centroid = geojson::Feature {
        geometry: Some(centroid_geometry),
        properties: Some(centroid_props),
        bbox: None,
        id: None,
        foreign_members: None,
    };
    let track_geom = geojson::Value::LineString(
        positions
            .iter()
            .map(|p| match p.alt {
                Some(alt) => vec![p.lon, p.lat, alt as f64],
                None => vec![p.lon, p.lat],
            })
            .collect(),
    );
    let mut track_props = serde_json::Map::new();
    track_props.insert("stroke".to_string(), "#ff0000".into());
    track_props.insert("stroke-width".to_string(), 2.into());
    track_props.insert("stroke-opacity".to_string(), 1.0.into());
    let track = geojson::Feature {
        geometry: Some(Geometry::new(track_geom)),
        properties: Some(track_props),
        bbox: None,
        id: None,
        foreign_members: None,
    };
    let features = vec![centroid, track];

    geojson::FeatureCollection {
        features,
        bbox: None,
        foreign_members: None,
    }
}

#[cfg(test)]
mod tests {
    #![allow(clippy::unwrap_used)]
    use super::*;

    #[test]
    fn test_file_timestamp_string() {
        let dt = Utc.ymd(2021, 1, 1).and_hms_milli(1, 2, 3, 1);
        assert_eq!(file_timestamp_string(dt), "20210101-010203-001");
    }

    #[test]
    fn test_file_icao_string() {
        assert_eq!(file_icao_string("ABC123"), "abc123");
        assert_eq!(file_icao_string("~ABC123"), "abc123");
    }

    #[test]
    fn test_base_filename() {
        assert_eq!(
            base_filename(
                &None,
                "~ABC123",
                Utc.ymd(2021, 1, 1).and_hms_milli(1, 2, 3, 1)
            ),
            "abc123-20210101-010203-001"
        );
        assert_eq!(
            base_filename(
                &Some("la".to_string()),
                "~ABC123",
                Utc.ymd(2021, 1, 1).and_hms_milli(1, 2, 3, 1)
            ),
            "la-abc123-20210101-010203-001"
        );
    }
}
