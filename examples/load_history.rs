use std::time::Instant;
use std::{
    env,
    fs::File,
    io::{BufReader, Read},
};

use advisorycircular::app_state::AppState;

pub fn read_file(filepath: &str) -> String {
    let file = File::open(filepath).expect("could not open file");
    let mut buffered_reader = BufReader::new(file);
    let mut contents = String::new();
    buffered_reader.read_to_string(&mut contents).unwrap();
    contents
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let path = args
        .get(1)
        .expect("Must supply path to history JSON file as argument");
    let start = Instant::now();
    let _ = AppState::from_file(path);
    println!("Read JSON in {} s", start.elapsed().as_secs_f32());
}
