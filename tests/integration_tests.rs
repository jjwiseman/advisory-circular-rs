mod tests {
    use advisorycircular::{
        aircraft::{curviness, Pos},
        app_state::AppState,
    };

    macro_rules! assert_approx_eq {
        ($x:expr, $y:expr, $eps:expr) => {
            let diff = ($x - $y).abs();
            if (diff > $eps) {
                panic!("\nleft: {}\nright: {}\n", $x, $y);
            }
        };
    }

    #[test]
    fn test_curviness() {
        let geojson_str = include_str!("circle.geojson");
        let positions = Pos::from_geojson_str(geojson_str).unwrap();
        assert_approx_eq!(curviness(&positions), 360.0, 3.0);

        let geojson_str = include_str!("lawnmower.geojson");
        let positions = Pos::from_geojson_str(geojson_str).unwrap();
        assert_approx_eq!(curviness(&positions), 0.0, 3.0);
    }

    #[test]
    fn test_save_history() {
        let hist = AppState::from_file("la-history.json").unwrap();
        hist.save_file("la-history.new.json").unwrap();
        let hist2 = AppState::from_file("la-history.new.json").unwrap();
        std::fs::remove_file("la-history.new.json").unwrap();
        assert_eq!(hist, hist2);
    }
}
